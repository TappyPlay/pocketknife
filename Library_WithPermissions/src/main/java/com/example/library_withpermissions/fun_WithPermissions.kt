package com.example.library_withpermissions

import android.Manifest
import android.content.Context
import android.widget.Toast
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
lateinit var toast: Toast


fun methodWithPermissions(mythis: Context) =
    mythis.runWithPermissions(Manifest.permission.ACCESS_FINE_LOCATION) {
        // Do the stuff with permissions safely
        // TODO fix the asynchronicity of this so that it blocks until permission is granted
        toast = Toast.makeText(mythis, mythis.getString(R.string.loc_granted), Toast.LENGTH_LONG)
        toast.show()
    }