package com.example.pocketknifeapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.distribute.Distribute;
import com.microsoft.appcenter.utils.async.AppCenterConsumer
import com.example.library_isitrooted.isItRooted
import com.example.library_getbluetoothstatus.getBlueToothStatus
import com.example.library_getlocationstatus.getLocationStatus
import com.example.library_getnetworkstatus.getNetworkStatus
import com.example.library_getmacaddress.getMACAddress
import com.example.library_withpermissions.methodWithPermissions

class MainActivity : AppCompatActivity() {

    lateinit var toast: Toast
    private lateinit var textMessage: TextView
    lateinit var button: Button
    internal val builder = StringBuilder()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        button = findViewById(R.id.button_Map_id)
        button.setText("Map");

        textMessage = findViewById(R.id.message)
        textMessage.setTextColor(Color.BLACK)
//        textMessage.setText("")//clear screen

        AppCenter.start(
            application, "d22cdd64-9c51-46aa-97e4-2adf0758f0a6",
            Analytics::class.java, Crashes::class.java, Distribute::class.java
        )

        val future = Crashes.hasCrashedInLastSession()
        future.thenAccept(AppCenterConsumer {
            if (it) {
                Toast.makeText(this, "Oops! Sorry about that crash!", Toast.LENGTH_LONG).show()
            }
        })

        methodWithPermissions(this)

        textMessage.setText(getNetworkStatus(this, builder))
        textMessage.setText(getLocationStatus(this, builder))
        textMessage.setText(getBlueToothStatus(this, builder))
        textMessage.setText(getMACAddress(this, builder))
        textMessage.setText(isItRooted(this, builder))

//        Map feature

        button.setOnClickListener {
//             Crashes.generateTestCrash()
            val intent: Intent = Intent(this, MapsActivity::class.java).apply {}
            startActivity(intent)
        }
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, getString(R.string.onStop), Toast.LENGTH_LONG)
    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, getString(R.string.onRestart), Toast.LENGTH_LONG)
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, getString(R.string.onResume), Toast.LENGTH_LONG)
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, getString(R.string.onStart), Toast.LENGTH_LONG).show()
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, getString(R.string.onPause), Toast.LENGTH_LONG)

    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, getString(R.string.onDestroy), Toast.LENGTH_LONG)
    }
}
